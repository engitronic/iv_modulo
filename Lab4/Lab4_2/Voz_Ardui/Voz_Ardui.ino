/* Este programa nos permitirá controlar el encendido y apagado de un led por comando de voz desde el celular.*/

char dato;       
void setup(){
  pinMode(8,OUTPUT);   // Pin 8 como Salida
  Serial.begin(9600);  // Abre el puerto
}

void loop(){
  if (Serial.available()>0){  // Verifica si hay dato disponible
      dato = Serial.read();    // Lee el puerto
  if (dato=='a'){
        digitalWrite(8,HIGH);  // Si es a, PIN8 = HIGH
  }
  if (dato=='b'){
        digitalWrite(8,LOW);   // Si es b, PIN8 = LOW
  }
   }
}


