/* Este programa nos permitirá controlar el encendido y apagado de leds a través de los datos recibidos del celular.*/

char dato;       
void setup(){
  pinMode(8,OUTPUT);   // Pin 8 como Salida
  pinMode(9,OUTPUT);   // Pin 9 como Salida
  pinMode(10,OUTPUT);  // Pin 10 como Salida
  Serial.begin(9600);  // Abre el puerto
}

void loop(){
  if (Serial.available()>0){  // Verifica si hay dato disponible
      dato = Serial.read();    // Lee el puerto
  if (dato=='a'){
        digitalWrite(8,HIGH);  // Si es a, PIN8 = HIGH
  }
  if (dato=='b'){
        digitalWrite(9,HIGH);  // Si es b, PIN9 = HIGH
  }
  if (dato=='c'){
        digitalWrite(10,HIGH); // Si es c, PIN10 = HIGH
  }
  if (dato=='d'){
        digitalWrite(8,LOW);   // Si es d, PIN8 = LOW
  }
  if (dato=='e'){
        digitalWrite(9,LOW);   // Si es e, PIN9 = LOW
  }
  if (dato=='f'){
        digitalWrite(10,LOW);  // Si es f, PIN10 = LOW
  }



if (dato=='g'){
        digitalWrite(8,HIGH);  // Si es g, PIN8 = HIGH
    digitalWrite(9,HIGH);  //          PIN9 = HIGH
    digitalWrite(10,HIGH); //         PIN10 = HIGH
  }
if (dato=='h'){
        digitalWrite(8,LOW);   // Si es h, PIN8 = LOW
    digitalWrite(9,LOW);   //       PIN9 = LOW
    digitalWrite(10,LOW);  //        PIN10 = LOW
  }
   }
}

